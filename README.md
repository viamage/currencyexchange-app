# CurrencyExchange

This app downloads Fixer currencies data every 5 hours (to not reach 1000 free limit) and provides old api methods with base and symbols.

Written in Lumen, no facades loaded, heavily cached - use Redis please. 

## Deployment

Clone the app and setup your .env file:

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=YOURDB
DB_USERNAME=YOURUSER
DB_PASSWORD=YOURPASS

CACHE_DRIVER=redis
QUEUE_DRIVER=sync

FIXER_KEY=YOURFIXERKEY 
```
Run composer update

```
$ composer update --prefer-dist
```

Run migrations
```
$ php artisan migrate
```

Seed with initial data and check if no errors:

```
$ php artisan currencies:update
```

If all went fine setup your cron:

```
$ crontab -e
```

with 

```
* * * * * php /path/to/app/artisan schedule:run
```


## API


```
[GET] /latest
```
Displays complete latest currencies ratios for default base (EUR)


**Parameters:**
```
base=ISO_CODE       - changes base from EUR to custom. Optional.
symbols=ISO1,ISO2   - comma separated ISOs, allowing to filter results. Optional. 
```

**Example:**
```
[GET] <domain>/latest?base=USD&symbols=EUR,CAD,PLN
```

**Response:**
```json
{
  "success": true,
  "timestamp": 1526294826,
  "base": "USD",
  "date": "2018-05-14",
  "rates": {
    "CAD": 1.27714,
    "EUR": 0.834298,
    "PLN": 3.560298
  }
}
```



```
[GET] /ratio
```

Provides direct result of recalculation. 

**Parameters:**
```
base=<ISO>          - source currency ISO code. Required.
target=<ISO2>       - target currency ISO code. Required.
amount=<integer>    - amount to convert. Optional (default = 1)
```

**Example:**
```
[GET] <domain>/ratio?base=USD&target=EUR&amount=23
```

**Response:**
```json
19.188854
```