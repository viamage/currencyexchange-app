<?php

namespace App\Http\Controllers;

use App\Archive;
use Cache;
use DB;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiController
 * @package App\Http\Controllers
 */
class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $cache;

    /**
     * ApiController constructor.
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    public function welcome(): JsonResponse
    {
        return response()->json(['success' => true, 'state' => 'online', 'message' => 'hello'], 200);
    }

    /**
     * @return JsonResponse
     * @throws \InvalidArgumentException
     */
    public function getLatest(): JsonResponse
    {
        $getData = $_GET;
        $base = 'EUR';
        if (array_key_exists('base', $getData)) {
            $base = $getData['base'];
        }
        $cacheKey = $base;
        $filter = [];
        if (array_key_exists('symbols', $getData)) {
            $cacheKey .= str_replace(',', '_', $getData['symbols']);
            $filter = explode(',', $getData['symbols']);
        }

        if ($this->cache->has($cacheKey)) {
            return response()->json($this->cache->get($cacheKey));
        }

        $result = $this->cache->rememberForever(
            'latest_archive',
            function () {
                return app('db')->table('archive')->orderBy('date', 'desc')->first();
            }
        );

        if (!$result) {
            return $this->buildNotFoundResponse();
        }

        return $this->buildArchiveResponse($result, $base, $filter, $cacheKey);

    }

    public function getRatio()
    {
        $getData = $_GET;
        $amount = 1.0000;
        if (!array_key_exists('base', $getData)) {
            return response()->json(['success' => false, 'error' => 'Missing base parameter'], 400);
        }
        if (!array_key_exists('target', $getData)) {
            return response()->json(['success' => false, 'error' => 'Missing target parameter'], 400);
        }
        if (array_key_exists('amount', $getData)) {
            $amount = (double)$getData['amount'];
        }
        $sourceIso = $getData['base'];
        $targetIso = $getData['target'];
        $source = $this->cache->rememberForever(
            'currency_'.$sourceIso,
            function () use ($sourceIso) {
                return app('db')->table('currencies')->where('iso', $sourceIso)->first();
            }
        );
        $target = $this->cache->rememberForever(
            'currency_'.$targetIso,
            function () use ($targetIso) {
                return app('db')->table('currencies')->where('iso', $targetIso)->first();
            }
        );
        if (!$source) {
            return response()->json(['success' => false, 'error' => $sourceIso.' not found'], 404);
        }
        if (!$target) {
            return response()->json(['success' => false, 'error' => $targetIso.' not found'], 404);
        }

        $sourceRatio = $source->ratio;
        $targetRatio = $target->ratio;

        $result = round($targetRatio / $sourceRatio, 6) * $amount;

        return response()->json($result, 200);
    }

    /**
     * @param \stdClass $archiveEntry
     * @param string    $baseCurrency
     * @param array     $filter
     * @param string    $cacheKey
     * @return JsonResponse
     */
    public function buildArchiveResponse(
        \stdClass $archiveEntry,
        string $baseCurrency = 'EUR',
        array $filter = [],
        string $cacheKey
    ): JsonResponse {
        try {
            $array = $this->cache->remember(
                $cacheKey,
                30,
                function () use ($archiveEntry, $filter, $baseCurrency) {
                    $json = $archiveEntry->json;
                    $array = json_decode($json, true);
                    $rates = $array['rates'];
                    if ($array['base'] !== $baseCurrency) {
                        $rates = $this->rebuildForNewBase($rates, $baseCurrency);
                    }
                    if (\count($filter) > 0) {
                        $rates = $this->filter($rates, $filter);
                    }
                    $array['base'] = $baseCurrency;
                    $array['rates'] = $rates;

                    return $array;
                }
            );

            return response()->json($array, 200);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     *
     * @throws \InvalidArgumentException
     */
    private function buildNotFoundResponse(): JsonResponse
    {
        return response()->json(['success' => false, 'error' => 'Not found'], 404);
    }

    /**
     * @param array  $currencies
     * @param string $baseCurrency
     * @return array
     */
    private function rebuildForNewBase(array $currencies, string $baseCurrency): array
    {
        $result = [];
        if (!array_key_exists($baseCurrency, $currencies)) {
            throw new \RuntimeException('Base not supported');
        }
        $baseRatio = (double)$currencies[$baseCurrency];
        foreach ($currencies as $iso => $ratio) {
            $result[$iso] = round((double)$ratio / $baseRatio, 6);
        }

        return $result;
    }

    /**
     * @param array $rates
     * @param array $filter
     * @return array
     */
    private function filter(array $rates, array $filter): array
    {
        return array_filter(
            $rates,
            function ($element) use ($filter) {
                return \in_array($element, $filter, true);
            },
            ARRAY_FILTER_USE_KEY
        );
    }
}
