<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/14/18
 * Time: 1:21 PM
 */

namespace App\Console\Commands;

use App\Archive;
use App\Currency;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateCurrencies extends Command
{

    const FIXER_URL = 'http://data.fixer.io/latest?access_key={{ACCESS_KEY}}';

    public $name = 'currencies:update';

    /**
     * This command echoes what you have entered as the message.
     * @return int Exit code
     * @internal param string $message the message to be echoed.
     */
    public function handle()
    {
        $ratesJson = $this->getRates();
        $date = Carbon::now();
        $this->updateCurrencyEntries($ratesJson, $date);
        $this->createArchive($ratesJson, $date);

    }

    private function getRates()
    {
        $accessKey = env('FIXER_KEY');
        $url = str_replace('{{ACCESS_KEY}}', $accessKey, self::FIXER_URL);

        return \Cache::remember(
            'latest_ratios',
            1440,
            function () use ($url) {
                return $this->sendGetRequest([], $url);
            }
        );
    }

    /**
     * @param array  $data
     * @param string $url
     * @return array|bool
     */
    public function sendGetRequest(array $data, string $url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    private function updateCurrencyEntries(string $ratesJson, Carbon $date): void
    {
        $updateCount = 0;
        $createCount = 0;
        $count = 0;
        $array = json_decode($ratesJson, true);
        if (array_key_exists('rates', $array)) {
            $total = \count($array['rates']);
            $this->info('Updating currencies...');
            $this->output->createProgressBar($total);
            $this->output->progressStart($total);
            foreach ($array['rates'] as $iso => $value) {
                $currency = Currency::where('iso', $iso)->first();
                if (!$currency) {
                    $currency = new Currency();
                    $currency->iso = $iso;
                    ++$createCount;
                } else {
                    ++$updateCount;
                }
                $currency->ratio = $value;
                $currency->updated_at = $date->toDateTimeString();
                $currency->save();
                \Cache::forget('currency_'.$iso);
                ++$count;
                $this->output->progressAdvance();
            }
            $this->output->progressFinish();
        }

        $this->info('Successfully updated '.$updateCount.' and created '.$createCount.' currencies');
    }

    private function createArchive(string $ratesJson, Carbon $date): void
    {
        $archive = Archive::where('date', $date->toDateString())->first();
        if (!$archive) {
            $archive = new Archive();
        }
        $archive->json = $ratesJson;
        $archive->date = $date->toDateString();
        $archive->save();
        $this->info('Successfully created archive');
        \Cache::forget('latest_archive');
    }
}