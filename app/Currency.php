<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{

    protected $table = 'currencies';

    public function getIso()
    {
        return $this->iso;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function getRatio()
    {
        return $this->ratio;
    }
}
