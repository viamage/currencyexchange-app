<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    protected $connection = 'mysql';
    protected $table = 'archive';

    public function getJson(): string
    {
        return $this->json;
    }

    public function getAsArray(): array
    {
        return json_decode($this->json, true);
    }

    public function getDate(): Carbon
    {
        return new Carbon($this->date);
    }

    public function setJson(string $json){
        $this->json = $json;
    }

    public function setDate(Carbon $date){
        $this->date = $date->toDateString();
    }
}
