<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    $controller = app()->make(\App\Http\Controllers\ApiController::class);
    return $controller->welcome();
});
$router->get('/latest', function () use ($router) {
    $controller = app()->make(\App\Http\Controllers\ApiController::class);
    return $controller->getLatest();
});

$router->get('/ratio', function () use ($router) {
    $controller = app()->make(\App\Http\Controllers\ApiController::class);
    return $controller->getRatio();
});